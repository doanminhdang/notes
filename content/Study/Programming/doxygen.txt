Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-02-20T16:28:39+01:00

====== doxygen ======
Created Montag 20 Februar 2017

http://fnch.users.sourceforge.net/doxygen_c.html

http://www.stack.nl/~dimitri/doxygen/manual/starting.html#extract_all


If you've formatted your comments in a compatible way, Doxygen does a fantastic job. It'll even draw inheritance diagrams if you've got graphviz installed.

For example, running DOxygen over the following:

/// <summary>
/// A summary of my class
/// </summary>
public class MyClass
{
protected:
	int m_numOfWidgets; /// Keeps track of the number of widgets stored

public:
	/// <summary>
	/// Constructor for the class.
	/// </summary>
	/// <param paramName="numOfWidgets">Specifies how many widgets to start with</param>
	MyClass(int numOfWidgets)
	{
		m_numOfWidgets = numOfWidgets;
	}

	/// <summary>
	/// Increments the number of widgets stored by the amount supplied.
	/// </summary>
	/// <param paramName="numOfWidgets">Specifies how many widgets to start with</param>
	/// <returns>The number of widgets stored</returns>
	IncreaseWidgets(int numOfWidgetsToAdd)
	{
		m_numOfWidgets += numOfWidgets;
		return m_numOfWidgets;
	}
};

Will turn all those comments into entries in .html files. With more complicated designs, the result is even more beneficial - often much easier than trying to browse through the source.

