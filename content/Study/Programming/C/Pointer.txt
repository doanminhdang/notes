Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2019-10-02T00:09:36+02:00

====== Pointer ======
Created Wednesday 02 October 2019

===== Initialize a string pointer =====

char s[] = "string" results in a modifiable array of 7 bytes, which is initially filled with the content  's' 't' 'r' 'i' 'n' 'g' '\0' (all copied over at runtime from the string-literal).

char *s = "string" results in a pointer to some read-only memory containing the string-literal "string".

If you want to modify the contents of your string, then the first is the only way to go. If you only need read-only access to a string, then the second one will be slightly faster because the string does not have to be copied.

https://stackoverflow.com/questions/4051347/in-c-can-i-initialize-a-string-in-a-pointer-declaration-the-same-way-i-can-init

Initialize a pointer to a number:

#include<stdio.h>
    
void main()
{
	int a = 10;
	int *ptr;       //pointer declaration
	ptr = &a;       //pointer initialization
}
https://www.studytonight.com/c/declaring-and-initializing-pointer.php

===== Create string of variable length =====
int  cLen     = 8;
char *str = malloc(cLen + 1);
memset(str, 'a', cLen);
str[cLen] = 0;
https://stackoverflow.com/questions/12352682/how-to-create-a-string-of-variable-length-with-character-x-in-c/12352926

#include <stdlib.h>
//declare a pointer to chars by
char *cpString;
//you ask for an allocation of "n" chars with
cpString=malloc(n*sizeof(char));

===== Receive string as parameter of a function =====

//Using pointer

#include<stdio.h>
#include<string.h>
 
data_type function_name (char *s){
//write your code.
}
 
int main(){
char *str1;
char str2[size];
 
scanf("%s",str1);
scanf("%s",str2);
 
data_type var1 = function_name(str1);
data_type var2 = function_name(str2);
//write your code here
return 0;
}

//Without using pointer.

#include<stdio.h>
 
data_type function_name (char s[]){
//write your code here
}
 
int main(){
/*declaring a string*/
char str[size_of_string];
 
/*for getting the string from standard input terminal.*/
scanf("%s", str); 
 
data_type var = function_name(str);
 
//Write your code
return 0;
}
https://www.quora.com/How-do-I-receive-string-as-parameter-in-C
