Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2016-12-05T18:38:13+01:00

====== auto ======
Created Montag 05 Dezember 2016

**auto is used to create a variable, usually a temporary variable, within a block of code**. The variable is only available to the block of code it has been defined within and cannot be referenced outside of the block of code in which it is being used.
For example:

	 for(;;) {
			   if getchar() == 'a') {
									  auto int i;
									  for (i=0; i<'a'; i++)
										   printf("%d , %c", i , i);
									}
			 }
In the example above, i is only defined if the if condition is true and only has value within the if block of code.

