Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2016-07-28T04:50:06+07:00

====== Stability of control ======
Created Thursday 28 July 2016

Nghiên cứu với Minh Lâm OlympiaVN:

Loại hàm:
x'(t) = a*x(t) + g1(y(t),z(t)), khi x>-c; và max(. , 0) khi x=-c <0
y'(t) = b*x(t) + g2(y(t),z(t))
z'(t) = c*x(t) + g3(y(t),z(t))
---
Do trường hợp không tính đến boundary thì hệ ổn định với mọi điểm ban đầu, xét điểm ban đầu có tọa độ x(0)=0, hệ vẫn ổn định (và sẽ tiến về equilibrium). Tức là sự biến đổi của g2() và g3() khi đi qua điểm x=0 vẫn luôn đảm bảo sự hội tụ ổn định.

* Xét trường hợp b<0, c<0, khi x(t) dừng lại ở biên x(t)=-c < 0:
-> b*x(t) >0, c*x(t) >0: ở lân cận điểm 0 sẽ có một lúc nào đó g2() trở nên quá nhỏ, cho nên y'(t) trong lân cận đó sẽ >0 khi x(t)=-c
-> lúc đó y(t) tăng dần

* Xét trường hợp a>0: có thể ở một lân cận đủ nhỏ của điểm 0, thành phần g1() có trị tuyệt đối quá nhỏ, khi đó nếu x=-c thì a*x(t) + g1() vẫn nhỏ hơn 0 => x(t) sẽ bị kẹt ở biên.
* Xét trường hợp a<0: ở một lân cận đủ nhỏ của điểm 0, thành phần g1() có trị tuyệt đối quá nhỏ, khi đó nếu x(t1)=-c thì a*x(t) + g1() lớn hơn 0 (kể từ thời điểm t>=t1+) => x(t) sẽ vượt ra khỏi biên. Lúc đó cần chứng minh: nếu nó quay trở lại biên thì trị tuyệt đối của g1() càng nhỏ hơn nữa.
Lưu ý dùng điều kiện exponential stability: điều kiện này rất mạnh.
Cần điều kiện: ||g1()|| tỉ lệ thuận với ||y()|| và ||z()||. Khi đó, nếu x(t2) quay trở lại điểm -c, thì do hàm exponential làm giảm ||x,y,z|| theo thời gian, làm cho ||x(t2),y(t2),z(t2)|| < ||x(t1),y(t1),z(t1)||
-> tùy loại norm, dù sao cũng sẽ suy ra ||y(t2),z(t2)|| < ||y(t1),z(t1)|| vì hai giá trị x(t1)=x(t2)=-c
-> tiếp tục dẫn đến ||g1(y(t2),z(t2))|| <= ||g1(y(t1+),z(t1+))||, mà a*x(t1+) + g1(y(t1+),z(t1+)) >0, nên a*x(t2) + g1(y(t2),z(t2)) >0, tức là ||g1(t2)|| không đủ mức âm để triệt tiêu đạo hàm x(t2) => x'(t2) > 0, nên sẽ lại thoát khỏi biên.
