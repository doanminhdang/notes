Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2015-11-22T10:31:31+07:00

====== Nghiên cứu về nước ======
Created Sunday 22 November 2015


1. Dự báo lũ trung hạn ở ĐBSCL (Quang)
Mô tả: dùng phần mềm mô phỏng để phục vụ phòng lũ, dự báo lũ trong thời gian khoảng 5 ngày. Thông tin lũ:
- Cấp báo động (theo mực nước)
- Dùng phần mềm dự báo lưu lượng, tra đường quan hệ lưu lượng - mực nước (hydro graph) -> ra được dự báo mực nước
Sử dụng mô hình giản lược (hiện Quang đã có mô hình trên Mike11)

Đã có:
- Công cụ tính toán mô phỏng (thuật toán, code phần mềm). (Quang sẽ chuyển Đăng xem phần mềm thử)
- Đã ứng dụng với test case ở ĐB sông Hồng

Cần làm thêm:
- Mực nước và lưu lượng ở các trạm chính (càng nhiều năm càng tốt). Cần chia bộ datataset làm hai bộ calibration và validation. (Đăng hỏi xin Đài khí tượng thủy văn Nam Bộ)
- Số liệu (theo giờ hoặc trung bình ngày) cần được chuyển thành structure trên Matlab / Octave. (Đăng)

Kết quả mong muốn:
- Publication: đăng báo quốc tế
- Ứng dụng: thông tin public trên trang web ở VN: vẽ ra flood hazard mapping (cho biết mỗi vùng ngập bao nhiêu, ngập thế nào) -> cho vào Geoportal để thể hiện trajectory lưu lượng & mực nước (xem trực tiếp tại vài mặt cắt, online, kiểu vnclimate / MRC có improved hơn). 

Proposal IFS: http://www.ifs.se/

2. Tính toán (dự báo) lưu lượng nước từ dữ liệu đơn giản (Đăng)
Mô tả: dùng mô hình lưu vực sông và dữ liệu đo mực nước -> lập thuật toán & phần mềm mô phỏng để tính ra lưu lượng nước
Sử dụng mô hình Mike11 cho vùng Tứ Giác Long Xuyên (Quang đã có). (có lẽ cần chuyển sang mô hình giản lược, để đưa lên web với Geoportal cho tiện)

Đã có:
- Mô hình với Mike11, Quang đã publish 3 bài báo
- Cảm biến đo mực nước (Đăng)
- Số liệu về lưu lượng và mực nước tại một số trạm (Quang)
- Mặt cắt ngang sông năm 2000 hoặc 2009 (hệ thống sông cấp 1 ở Tứ giác Long Xuyên, đo mặt cắt khoảng 500m một lần). Nếu làm ở vùng nhỏ hơn thì cần đo đạc thêm mặt cắt của sông cấp 2.

Cần làm thêm:
- Cảm biến đo lưu lượng nước loại đơn giản (Đăng)
- Bể nước mô hình thu nhỏ một khu vực sông/kênh và gắn thiết bị đo đạc trong phòng thí nghiệm (Đăng)
- Lập mô hình của khu vực cần nghiên cứu: cần điều kiện biên (lưu lượng, at least mực nước và đường quan hệ lưu lượng / mực nước - có thể phải dành một phần việc đo mặt cắt để thiết lập đường quan hệ), Quang cần số liệu lịch sử để calibrate cho mô hình
- Cần học cấu trúc của Geoportal để đưa data cho phù hợp

Kết quả mong muốn:
- Publication: đăng báo quốc tế
- Ứng dụng: tính toán tự động lưu lượng tại một số điểm (có thể cần liên lạc với bên nào làm về WebGIS, hoặc liên kết với Geoportal của Quang)

Geoportal cho phép mình đưa luồng dữ liệu ở backend, người xem có thể click vào các điểm trên trang web, và click vào simulation để chạy simulator để mô phỏng. Geoportal không phù hợp dùng với Mike11, cần dùng mô hình giản lược để mình tự code.

---
Các vấn đề cần xem xét (có làm không, như thế nào)
- Tìm sinh viên Master ở VN để làm nghiên cứu trực tiếp
- Đưa một phần công việc sang nhóm anh Trí
- Kết nối với Đài khí tượng thủy văn để họ cho người cùng làm

---

Họp 19/3/2016:

Quang:
- MRC có dự báo lũ cho Tân Châu và Châu Đốc thôi.
- Cần Thơ, Mỹ Thuận, Vàm Nao chưa có cảnh báo lũ theo thời gian thực -> dùng phần mềm của Quang làm được.
- Làm bản đồ ngập lụt.
- Xâm nhập mặn, chất lượng nước.
- Hiện có mô hình thủy lực: mực nước, lưu lượng thôi. Sau này cần làm thêm để có thể thêm thông số khác.
- Cần điều kiện biên qua sensor, và một số điểm đo ở giữa để kiểm định mô hình, ví dụ Tứ giác Long Xuyên.
- 

Users:
- Người ra quyết định cấp tỉnh trở lại
- MRC
- Trung tâm phòng chống lụt bão (chưa có lưu vực Mekong do thiếu nguồn lực)
Mỗi năm Bộ Nông nghiệp ký hợp đồng với Viện khoa học thủy lợi. Từ 2011 đến giờ.
