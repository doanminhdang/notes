Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2016-07-28T19:19:09+07:00

====== Discrete optimization (combinatorial opt) ======
Created Thursday 28 July 2016

Traveling sales man problem (TSP):

Website of William Cook at University of Waterloo: keep update about the TSP.
Concorde (by Pavel Striz) is available on NEOS.
QSopt (open-source) is even comparable to CPLEX.

Hans said the benchmark on TSP website of William Cook was outdated (using the software from 99) -> "totally useless".

Continuous LP:
Check benchmark lpsim on Plato website, the open-source solver CLP achieved very good result => some people didn't believe, Hans asked the author and he said: first he used heuristic to get a good initial point, then use exact algorithm to continue the search.

2016.08.04:
https://developers.google.com/optimization/
This site provides an introduction to or-tools, Google's software suite for combinatorial optimization.
