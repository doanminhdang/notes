Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-02-23T15:09:04+01:00

====== ESP32 ======
Created Donnerstag 23 Februar 2017

Tuấn ơi, cái ESP32 nếu lập trình bằng C thì nó dùng phiên bản C nào nhỉ? Có hỗ trợ C99 không em?

Có anh, ansi c, c99
Thanks em
C11 thì không phải không?

Để e check lại
Cái C99 hình như các chip embedded đời cao mới hỗ trợ, còn lại là Ansi C phải ko?
Kiểu AVR, PIC 16, 18 là ansi C. Pic 32 có hỗ trợ c99?

E ko rành lắm về các chuẩn c
Thường thì do compiler
Esp32 dùng crossng
Bọn anh đang chuẩn bị phát triển code hướng tới embedded platforms, đang khảo sát xem cái nào được support
Để anh check thêm esp32

Ah
Hình như support c11 luôn
Chuẩn bị làm cái như là hôm nọ anh post lên nhóm Kỹ sư thực thụ, lập các code giải ma trận và tính toán tối ưu

E có xem qua **crosstool-ng**
A chọn esp32 là đúng rồi
E đang ở bên espressif
Nó add thêm psram bên ngoài, nâng dung lượng ram lên 4mb luôn
Hỗ trợ arduino là quan trọng nhất
Còn ROM vẫn hơn 400kB thôi à?

Rom tới 64mb luôn
Hỗ trợ arduino dễ ship app tới user
App và lib
Do nó mở rộng thêm hả?

Nó dùng chip nhớ ngoài
Cái kit hardware đó ra chưa?

Ram cũng có thể mở rộng
Chưa a 😄
Đang dev mà
:D
Mấy tháng nữa có em?

Tầm 1 tháng nữa thôi
Khi đó thì các bo như Nano32 là phải revise à?

Ko a
Nó làm nhiều phiên bản mà
Có càng nhiều thì càng mắc
Giờ em có bo nào kiểu Nano32 dư không?
Anh muốn thử tìm phần cứng ESP32 để chạy chương trình xem sao
Theo anh hiểu là khi dùng toolchain của Espressif, không cần esp32 vẫn ra binary của nó được, phải ko?
Nếu có thể có cách setup nào mà anh chỉ cần SSH vào PC, xong load code xuống chip ESP32, chạy code và xem được output của code qua cửa sổ SSH của PC thì đỡ quá

Ok a, e có cả đống
Esp32 có qemu
A thử xem
Qemu là toolchain hay gì em?

Là simulator
Em viết hướng dẫn cài đặt để làm qemu trên esp32.vn đi
Cái toolchain mà Espressif hướng dẫn chắc anh làm theo được rồi

**https://github.com/Ebiroll/qemu_esp32**
**Ebiroll/qemu_esp32**
qemu_esp32 - Add tensilica esp32 cpu and a board to qemu and dump the rom to learn more about esp-idf
github.com
Thánng 12 này e ở thượng hải
Làm ko ngóc đầu dậy đc luôn
Hehe
Ko có thời gian viết lách gì


À, giữa openwrt với freertos thì cái nào dễ extend chức năng vậy em?

tùy a
thằng opnewrt mạnh hơn nhiều
nó là linux mà
còn freertos thì dùng cho mbedded



Bữa trước em bảo là bo cho esp32 mà mở rộng ROM 64, RAM 4M, cũng sắp release rồi
Giờ vẫn chỉ đang cho dev của espressif à?

nó bán rồi a
https://www.olimex.com/Products/IoT/ESP32-WROVER-KIT/
devkit
chứ ko bán module
tầm tháng 2-3 mới bán module
Không thấy ghi về vụ mở rộng rom với ram

hehe, nó có sẵn trong module đó rồi
32Mbits RAM + 64Mbits FLASH
Cái em định sẽ đưa anh là cái đó hay khác nữa?

e đưa a 2 cái
1 cái là nano32, 32 Mbits Flash + internal 512Kb RAM
1 cái là module 32Mbits RAM + 64Mbits FLASH
Cái thứ hai là cái đó hả em?
Có gì anh mua thêm để ở đây anh cho người vọc

http://esp32.de/ESP32_MODULE_2COL_.png
ko a
cái thứ 2 đây
vì e có duy nhất 1 dev kit
https://iotmaker.vn/nano32.html
Mạch phát triển ứng dụng IoT NANO32 - ESP32 - WIFI - BLE
Mạch phát triển ứng dụng IoT NANO32 - ESP32 - WIFI - BLE
iotmaker.vn
cái thứ nhất
Cái thứ hai là module mà olimex xài hả?

đúng đó a
Thanks em
Để anh mua thêm cả kit bên này
Em xem lại link của olimex cho anh cái, nó bảo là dùng cái ESP32-WROOM32 module
Mà cái module này cũng chỉ có 512 kB ram
https://www.olimex.com/Products/IoT/ESP32-WROOM-32/
ESP32-WROOM-32
ESP32-WROOM-32 WIFI/BLE module
olimex.com

à à
Có chắc dev kit đó có 32 Mb ram không?

chính xác đó a
nhưng có pinout tương tích với cái module e gởi cho a
tháo nó ra gắn vô thôi
😄
cái kia chưa bán ra ngoài đâu
PSRAM còn bug hardware
dùng để dev thì ok
Thế thì cũng ok, mà module đó không phải SMD à?
Hay cần khò nó ra hàn lại?

khò ra hàn lại a
e nghĩ dev thôi thì chỉ cần internal ram là nhiều rồi
khởi động hết network còn dư 120Kb RAM
À anh cần dev cho ứng dụng chạy rất tốn Ram
Nên huy động Ram càng lớn càng tốt

cái PSRAM giờ có vấn đề khi chạy với 2 core
digital team espressif fix xong rồi, đợt hàng tới mới ngon
Cái có PSRAM là được đến bao nhiêu ram em?

4Mbytes
Đó là bản 32 mbit = 4mb à?
Ok, tức là module mà em sẽ đưa anh, sau này họ fix lại rồi đưa ra thị trường hả?

hiện tại họ chỉ dùng để dev
sau này sẽ sản xuất hàng loạt
giống hệt vậy


Tuấn ơi, cái module esp32 em đưa anh là có datasheet hay schematic pin out không?

có a, đợi e chút
Anh mua bo Wroover này là đổi module đó vào được phải không? https://www.olimex.com/Products/IoT/ESP32-WROVER-KIT/
ESP32-WROVER-KIT
ESP32-WROVER-KIT development board for ESP32 with JTAG and LCD display, Camera interface, SD card, RGB LED, IO expander
olimex.com
Em gửi anh datasheet cái đó để anh check thêm cái

https://pbs.twimg.com/media/Cui-rKMXgAAoMj6.jpg
doc thì dùng esp-idf http://esp-idf.readthedocs.io/en/latest/
Search datasheet của module Wroom 32 thì file từ espressif cũng đang bị lỗi gì đó
Không phải nano32, cái 4MB ram ấy

à
cái đó chưa có datasheet gì đâu a
nhưng a cần dùng 4MB ram chưa?
Có sơ đồ pin out không?
Sắp cần, vì còn đặt mua cái bo từ Olimex để thay vào
Nếu đầu tháng 3 có cái bo cho 4MB hoàn chỉnh thì anh đợi được


đầu tháng 3 sợ chưa có đâu
khoảng 1 tuần nữa mới có chip
khi dùng psram, thì io16/17 ko dùng được nha a
Ok em


a mua cái board của olimex, rồi thay vô
bình thường khi a dùng wifi
thì esp32 còn dư tới 150kb internal RAM
a dùng ứng dụng gì mà cần nhiều thế?
Giải toán tối ưu
Vừa cần Ram, vừa cần double float
150kb thì chắc chắn thiếu rồi


ghê vậy à, e build ứng dụng dùng tới từng đó RAM là e thấy khủng lắm rồi


Mà nếu em không dùng wifi thì còn bao nhiêu ram?


khoảng 200Kb
e chưa thử disable hết, nó có tổng là 520Kb
mà chắc nó dùng cho việc gì e cũng chưa tìm hiểu
có thể phân vùng execute code
Nghe chừng cấu trúc phức tạp nhỉ  
À, nó compile code c++ ok không?


c++11 luôn a
thằng Olimex lại bị Out of stock cái bo Wrover


a dùng cái nano32 xài đỡ coi thử
Cơ chế để nó dùng code dựa trên lib kiểu này có làm được không em:
Anh có code cpp để tạo ra lib, cung cấp vài hàm, rồi có code C sử dụng các hàm trong lib kia


à, đương nhiên
cái idf nó làm hay lắm
a setup code a như 1 component
khi biên dịch nó tạo ra lib
Như trên PC thì cần tạo ra file object ấy


a chỉ cần separated file .h và file .cpp
khi biên dịch trong thư mục build
nó sẽ tạo file lib cho mỗi component
a copy cái lib đó + với đống header file
bỏ qua project khác
chạy ào ào
http://esp-idf.readthedocs.io/en/latest/build-system.html
Cuối cùng nạp lên chip là chép tất cả các file lib + execution lên à?

khi nạp lên chip thì nó link thành 1 file bin
có thể phân bổ ra nhiều partition
sau đó a setup phân vùng nào cho app nào
nhưng lúc đó phải sửa linker
khá phức tạp
e hiểu ý a muốn nạp 1 thư viện nào đó lên chip
xong rồi cung cấp api để gọi từ app khác
như kiểu softdevice của nordic?
Không, anh cần 1 cái executable thôi
Khi nó link ra file bin thì là chạy được rồi, không cần copy thư viện rời thêm phải không?


đúng rồi a
thậm chí a có thể setup ota, để cập nhật nó
Như anh test trên PC, thì lib chỉ là dùng tạm, khi compile file binary cuối xong thì anh xoá lib được


đúng rồi a
make xong nó tạo file bin
make clean nó xóa hết
Thế tốt lắm
À, anh có 1 chú sinh viên làm cùng ở đây, người Việt
Anh định đưa bạn ấy cái Nano32 để test code
Có gì bạn ấy cần thì hỏi em được không?
Anh sẽ giới thiệu qua Facebook


ok a
cơ bản thì doc của esp-idf khá đầy đủ rồi
Cám ơn em trước


e làm 1 phần trong đó mất hơn nửa năm nay
Cũng không biết là cấu trúc code của bọn anh chạy được không




nếu a có ý định opensource, e sẽ hỗ trợ implement
Giờ bạn ấy compile thử trên chip Arm cortex m4 của TI thì trầy trật
Thì bọn anh cũng chỉ làm open source thôi


e chuyên đi port mấy lib audio của linux, cũng hoành tráng lắm
chạy được trên linux thì đa phần là chạy được trên esp32
Cái của TI nó dùng tool của Arduino, compile bộ code vừa cpp vừa c, chưa biết thế nào


arduino cũng dùng c compiler bình thường thôi
Tức là cũng compile kiểu cpp ra lib rồi link được hả?


được hết a
c và cpp nó mix nhau loạn xạ
Trước giờ anh xài Arduino mới chỉ dừng ở code C trong sketch.ino


Arduino thực chất dùng c compiler
nó sử dụng giao diện và ide viết trên java
Vậy code cpp nó compile thế nào?


đồng thời tích hợp lib sming
thì cpp -> obj, c ->obj, lib.a + obj -> bin
flow y như C compiler bình thường
cpp và c có thể mix được với nhau
a xem cái esp-idf, có project mẫu mix cả 2
Tuy nhiên như ở linux mà gặp code cpp thì anh dùng g++ để dịch
Chứ chỉ gcc thì chắc không được
Arduino nó làm sao để cpp ra obj được?


nó dùng gcc++ -> obj
nếu file c nó dùng gcc -> obj
rồi dùng gcc ++ linker obj -> exec
bộ compiler đi kèm của gcc bao giờ cũng có gcc và gcc++
về cơ bản, build system của Arduino là bê nguyên cái C/C++ compiler
chỉ automatic, tự sinh Makefile thôi
Thế nó tự biết chọn cái nào à


đúng rồi a, dựa vào đuôi file
khi a biên dịch, hệ thống sẽ sinh Makefile
Nó quét cả thư mục rồi compile từng file phải không?


đúng a
Makefile nó có các hàm scan thư mục, filter ...
Nó có scan vào thư mục con được không em?
Recursive ấy


IDE sẽ làm việc đó
Trước giờ e thấy ít người cho Makefile scan recursive
mặc dù là có thể được
Người ta khai báo trong Makefile thư mục nào cần compile thôi
Uh, kiểu đó
Nhưng chú Arduino tự tạo makefile
Nên không dùng makefile có sẵn ở PC được


à
có dự án
dùng Makefile cho arduino
a hoàn toàn không cần arduino ide để build
Tên gì em nhớ không?


https://github.com/thunderace/Esp8266-Arduino-Makefile
bắt chước nó làm thôi
chưa kể có 1 dự án khác cũng lớn
là platform.io
cũng có thể biên djch arduino code dễ dàng
Thanks em nhé

Thôi chúc em ngủ ngon  


http://platformio.org/
PlatformIO: An open source ecosystem for IoT development
Cross-platform build system and library manager. Continuous and IDE integration. Arduino and ARM mbed compatible. Ready for Cloud compiling.
platformio.org
e gởi nhầm thằng
bb
Bb em
Mà thằng TI nó chỉ dùng tool IDE của Arduino (tên là Energia)
Có lẽ nếu học cách làm makefile kiểu project kia thì vẫn cần setup đúng tham số cho loại chip của TI nhỉ

a xem chip loại nào
dùng compile nào
Bọn anh Anh đang làm với cái này: http://energia.nu/pin-maps/guide_tm4c123launchpad/
Guide_TM4C123LaunchPad
Guide to the TM4C123 LaunchPad (EK-TM4C123GXL) Supported in Energia version 10+ The EK-TM4C123GXL LaunchPad is a very capable board with a 32-bit 80MHz ARM Cortex-M4F processor. It has many periphe…
energia.nu

con đó bé tẹo mà?
có 32kb ram, chạy 80mhz
Con này chứ: http://www.ti.com/lit/ds/symlink/tm4c129encpdt.pdf
www.ti.com
ti.com
Nhầm con, chỉ là dùng Energia kiểu đó thôi

E nghĩ nếu a làm với ESP32, không cần arduino
esp-idf của nó thiết kế rất tốt
dùng c và cpp
module hóa cao
có thể phát triển ứng dụng lớn
nếu làm arduino sẽ ko tận dụng được ưu thế của freertos
Ngoài ra, e không có thiện cảm lắm với bọn làm chip công nghiệp như TI và MCHP
lý do a cũng thấy đó
bọn nó rất tham lam
Mắc tiền hả?


làm nhiều thứ, đẻ ra nhiều thứ trên trời, đóng kín như bưng, và chất lượng software thì không đồng đều
Tuy nhiên bọn nó tốn nhiều tiền cho testing để chạy được trong công nghiệp


nhiều khi các vấn đề đó
chỉ là mục đích để bọn nó ép thằng khác theo
Uh, đồng ý


e làm nhiều các sản phẩm liên quan tới công nghiệp e biết
hiện tại e ủng hộ mấy thằng phát triển mở
bao gồm cả espressif
zephyr
risc v
a ở Đức, mua chip của Risc V về xài
Những thằng đó sẽ giúp kéo nền công nghệ của cả thế giới đi rất nhanh
như dạng phổ cập công nghệ cao
Uh
Anh chưa biết risc V
Zephyr thì thấy mấy công ty support mạnh ở đợt hội nghị embedded vừa rồi


risc v open hardware, e đang đặt mua chip về nghịch
A xài chip nào
mà mấy cái project đó đang và sẽ support
Em chọn cái nào từ Risc V?


https://www.sifive.com/products/hifive1/
HiFive1
SiFive is the first fabless semiconductor company to build customized silicon based on the free and open RISC-V instruction set architecture.
sifive.com
Chỉ để làm microcontroller thôi nhỉ

nhưng nó open sillicon
tầm 1-2 năm nữa đầy thằng sẽ dùng nó
rồi nhét đủ thứ trên đời vô
Như ở VN chẳng hạn
ICDREC cần gì phải thiết kế core cho mệt
lấy nguyên cái core đó về, bổ sung các periph vô
là ra chip
đỡ mất công làm compiler, debugger, ide...
Uh, đột nhiên mấy chục tỉ cho Icdrec vô nghĩa liền nhỉ  


chưa kể, sau khi thiết kế chip xong
ở VN không có nhân lực đủ trình độ để port mấy cái như newlib
các thư viện chuẩn của C để dùng
os cho core đó
mà các thể loại như nước VN này trên trái đất nhiều lắm






e tin là các dự án open này sẽ phát triển rất mạnh mẽ
https://riscv.org/specifications/
để làm ra con chip cần hàng tấn thứ phải làm
icdrec chưa đủ năng lực thôi, chứ ko phải họ ko muốn làm
