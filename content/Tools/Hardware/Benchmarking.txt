Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2016-11-03T12:16:21+01:00

====== Benchmarking ======
Created Donnerstag 03 November 2016


Table: CPU and GPU

Device	Processor	Architecture	CPU Speed	Cores	GPU
Raspberry Pi 2	Broadcom BCM2836	ARM Cortex-A7	900 MHz	4	Broadcom
BeagleBone Black Rev C	TI Sitara AM335x	ARM Cortex-A8	1 GHz	1	PowerVR SGX530
Intel Edison	Intel Atom Z34XX	Silvermont x86	500 MHz	2	None
pcDuino3	Allwinner A20	ARM Cortex-A7	1 GHz	2	Mali-400MP2
Acadia	Freescale i.MX6	ARM Cortex-A9	1.2 GHz	4	Vivante GC2000

https://learn.sparkfun.com/tutorials/single-board-computer-benchmarks

ODROID-XU4 là trùm. Rasperry Pi 3 rất mạnh, mạnh hơn Odroid-C1 và giá cũng rẻ hơn (RPi 3 nhỉnh hơn Odroid-C1 một chút ở mọi tiêu chí). Orange Pi hiệu năng cũng gần bằng Odroid-C1 và giá rẻ hơn nhiều.

For new installations it is strongly recommend investing in multi-core (at least 2 cores) hardware, as single-core mini boards may have problems dealing with intensive traffic. If in addition to OGN receiver software you plan to run other software on the same hardware (e.g. you are active feeder of ADS-B data to FR24), then definitely do not consider any single-core solution.

If, however, you already have one or you can get one really cheap - for now we recommend to go for Raspberry Pi! because of two main reasons:

there is a very convenient installation procedure available for R. Pi (thanks to Mell's image)
OGN receiver software is able to use GPU on Raspberry Pi (and that significantly improves performance, although e.g. BeagleBone Black without the use of GPU proves to offer similar performance as Pi with GPU)
http://wiki.glidernet.org/cpu-boards

OGN?
- open protocol
- tracking device
- is cheap! Making the hardware easily affordable

http://wiki.glidernet.org/ogn-tracker
The objective of the Open Glider Network is to create and maintain a unified tracking platform for gliders and other GA aircraft. Currently OGN focuses on tracking aircraft equipped with FLARM, FLARM-compatible devices or OGN tracker.

When Pi1 was out, the BeagleBone Black with the more modern Cortex-A8 chip and higher clockrate was definitely the more powerful, but now with 4-core Pi2, the tables have somewhat turned. Still, the clockrate is higher and there’s more GPIO.
http://codeandlife.com/2016/04/18/beaglebone-black-gpio-benchmark/

AMD Sempron 2100+ (1GHz) : better performance than Atom N270 (1.6 Ghz)
However, Atom N270 is more energy efficient: 2.5W, Sempron 2100+: 8W
http://cpuboss.com/cpus/Intel-Atom-N270-vs-AMD-Sempron-2100

AMD Mobile Sempron 2100+ advantages:
Thanks to integrated AMD64 feature, this chip can run 64-bit operating systems.
Intel Atom N280 advantages:
One of the distinctive features of the processor is Hyper-Threading technology. This feature allows each CPU core to execute two threads simultaneously.
http://www.cpu-world.com/Compare/386/AMD_Mobile_Sempron_2100+_vs_Intel_Atom_N280.html

---
2016.11.14

Ket qua Dang benchmark may Thin client Fujitsu Futro S550 (AMD Sempron 2100+ 1GHz, RAM 1GB), theo bai test o day, chay Ubuntu 14.04.04 tren USB: http://wiki.glidernet.org/cpu-boards

$ sysbench **--num-threads=1** --test=cpu --cpu-max-prime=2000 run

Test execution summary:
 **total time: 8.9729s**
 total number of events: 10000
 total time taken by event execution: 8.9659
 per-request statistics:
   min: 0.87ms
   avg: 0.90ms
   max: 8.91ms
   approx. 95 percentile: 0.88ms

Threads fairness:
 events (avg/stddev): 10000.0000/0.00
 execution time (avg/stddev): 8.9659/0.00

$ sysbench **--num-threads=2** --test=cpu --cpu-max-prime=2000 run

Test execution summary:
 **total time: 8.9830s**
 total number of events: 10000
 total time taken by event execution: 17.9520
 per-request statistics:
   min: 0.87ms
   avg: 1.80ms
   max: 17.78ms
   approx. 95 percentile: 4.88ms

Threads fairness:
 events (avg/stddev): 5000.0000/2.00
 execution time (avg/stddev): 8.9760/0.00

$ sysbench **--num-threads=4** --test=cpu --cpu-max-prime=2000 run

Test execution summary:
 total time: 8.9726s
 total number of events: 10000
 total time taken by event execution: 35.8414
 per-request statistics:
   min: 0.87ms
   avg: 3.58ms
   max: 35.99ms
   approx. 95 percentile: 12.88ms

Threads fairness:
 events (avg/stddev): 2500.0000/1.22
 execution time (avg/stddev): 8.9604/0.00

$ sysbench --test=memory --memory-block-size=**1M** --memory-total-size=**256M** run

256.00 MB transferred (**771.98 MB/sec**)

Test execution summary:
 total time: 0.3316s
 total number of events: 256
 total time taken by event execution: 0.3303
 per-request statistics:
   min: 1.10ms
   avg: 1.29ms
   max: 9.15ms
   approx. 95 percentile: 1.74ms

Threads fairness:
 events (avg/stddev): 256.0000/0.00
 execution time (avg/stddev): 0.3303/0.00

$ sysbench --test=memory --memory-block-size=**512K** --memory-total-size=**512M** run

512.00 MB transferred (**799.95 MB/sec**)

Test execution summary:
 total time: 0.6400s
 total number of events: 1024
 total time taken by event execution: 0.6375
 per-request statistics:
   min: 0.53ms
   avg: 0.62ms
   max: 8.86ms
   approx. 95 percentile: 0.60ms

Threads fairness:
 events (avg/stddev): 1024.0000/0.00
 execution time (avg/stddev): 0.6375/0.00
