Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2015-03-02T02:58:11+07:00

====== Odroid ======
Created Monday 02 March 2015

Cuối 2014, họ ra Odroid-C1 giá 35$, chạy chip 4 nhân, có ADC:
http://www.hardkernel.com/main/products/prdt_info.php?g_code=G141578608433
-> rất mạnh hơn Raspberry Pi.

Mua ở Mĩ, giá 37$: STORE IN US : http://ameridroid.com/
(sau đó có thể dùng dịch vụ vận chuyển của Thanh Vân O1 để chuyển về SG)

Giả sử mua 1 bo, cho là cả hộp hết 100g: 37+10% thuế+0.4*2.75 tiền vận chuyển = 42$ về tới SG
