Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2019-06-12T14:07:27+02:00

====== Unit test ======
Created Wednesday 12 June 2019

Use pytest: it is simple, automated test runner.

Any test file should have the file name starts with "test_". In each test file, there could be multiple functions, in each function we put the command `assert` to test the results.

For running the unit tests, go to the top directory of the project, run on Console:

pytest
or:
**pytest -vv**
(to see the detailed report, with result on each testing function, and diff between unmatching results of the assert commands)
