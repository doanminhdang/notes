Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2016-11-18T20:14:52+01:00

====== Tinycore ======
Created Freitag 18 November 2016

**Use Apps   to remove programs**

Open Apps, select  Apps button > Maintenance >  Dependencies and Deletions, wait for the list of installed extensions to be built, then,
Select the Extension you wish to remove, while highlighted go up to the Dependencies button and choose "Mark for deletion"  the extension will be listed in the right window along with any errors messages regarding dependencies..
Next select the Dependencies button > Exit Dependency check> close Apps, Apps button > Quit, then reboot

Ref: http://forum.tinycorelinux.net/index.php/topic,17584.15.html

**Create extensions (aka. package) for Tinycore:**
http://wiki.tinycorelinux.net/wiki:creating_extensions
http://forum.tinycorelinux.net/index.php?topic=18682.0
(not try yet)
Ref: http://forum.tinycorelinux.net/index.php?topic=17354.0

**Dùng scp với dropbear**:
Sau khi cài và chạy dropbear lần đầu, nó tạo ra file RSA & DSS keys, trong thư mục /usr/local/etc/dropbear, đang ở trong RAM -> cần lưu các file đó vào ổ cứng để dùng lần sau.
Dùng SSH keys để authorize: cho các keys vào file ~/.ssh/authorized_keys (giống như với OpenSSH)
Để dùng scp với dropbear: do Tinycore lưu file scp ở địa chỉ /usr/local/bin/scp, mà khi client kết nối scp với Tinycore thì nó tìm scp ở địa chỉ /usr/bin/scp, nên cần làm symlink:
$ sudo ln -s /usr/local/bin/scp [[/usr/bin/scp]]

