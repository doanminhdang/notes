Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2015-10-11T17:03:42+07:00

====== Julia ======
Created Sunday 11 October 2015

You can use Julia via Sage Worksheets by putting %julia at the top of a cell or by putting "%default_mode julia" somwhere to switch everything to use Julia. 
http://ask.sagemath.org/question/11021/julia-and-ijulia-on-sage-math-cloud/

Có thể mình cũng gọi được Octave trong Sagemath bằng cách đặt %octave at the top of a cell, hoặc %default_mode octave", để khi compile Sagetex thì nó gọi Octave.
