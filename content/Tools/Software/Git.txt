Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2016-07-14T18:53:45+07:00

====== Git ======
Created Thursday 14 July 2016

Tóm tắt ngắn gọn, dễ đọc:
http://rogerdudler.github.io/git-guide/

create a new repository
create a new directory, open it and perform a 
**git init**
to create a new git repository.

checkout a repository
create a working copy of a local repository by running the command
**git clone /path/to/repository**
when using a remote server, your command will be
**git clone username@host:/path/to/repository**

workflow
your local repository consists of **three "trees"** maintained by git. the first one is your Working Directory which holds the actual files. the second one is the Index which acts as a staging area and finally the HEAD which points to the last commit you've made.

add & commit
You can propose changes (add it to the Index) using
**git add <filename>**
**git add ***
This is the first step in the basic git workflow. To actually commit these changes use
**git commit -m "Commit message"**
Now the file is committed to the HEAD, but not in your remote repository yet.

pushing changes
Your changes are now in the HEAD of your local working copy. To send those changes to your remote repository, execute 
**git push origin master**
Change master to whatever branch you want to push your changes to. 

If you have not cloned an existing repository and want to connect your repository to a remote server, you need to add it with
**git remote add origin <server>**
Now you are able to push your changes to the selected remote server
**branching**
Branches are used to develop features isolated from each other. The master branch is the "default" branch when you create a repository. Use other branches for development and merge them back to the master branch upon completion.

create a new branch named "feature_x" and switch to it using
**git checkout -b feature_x**
switch back to master
**git checkout master**
and delete the branch again
**git branch -d feature_x**
a branch is not available to others unless you push the branch to your remote repository
**git push origin <branch>**

update & merge
to update your local repository to the newest commit, execute 
**git pull**
in your working directory to fetch and merge remote changes.
to merge another branch into your active branch (e.g. master), use
**git merge <branch>**
in both cases git tries to auto-merge changes. Unfortunately, this is not always possible and results in conflicts. You are responsible to merge those conflicts manually by editing the files shown by git. After changing, you need to mark them as merged with
**git add <filename>**
before merging changes, you can also preview them by using
**git diff <source_branch> <target_branch>**

tagging
it's recommended to create tags for software releases. this is a known concept, which also exists in SVN. You can create a new tag named 1.0.0 by executing
**git tag** 1.0.0 1b2e1d63ff
the 1b2e1d63ff stands for the first 10 characters of the commit id you want to reference with your tag. You can get the commit id by looking at the... 

log
in its simplest form, you can study repository history using.. **git log**
You can add a lot of parameters to make the log look like what you want. To see only the commits of a certain author:
**git log --author=bob**
To see a very compressed log where each commit is one line:
**git log --pretty=oneline**
Or maybe you want to see an ASCII art tree of all the branches, decorated with the names of tags and branches: 
**git log --graph --oneline --decorate --all**
See only which files have changed: 
**git log --name-status**
These are just a few of the possible parameters you can use. For more, see git log --help

replace local changes
In case you did something wrong, which for sure never happens ;), you can replace local changes using the command
**git checkout -- <filename>**
this replaces the changes in your working tree with the last content in HEAD. Changes already added to the index, as well as new files, will be kept.

If you instead want to drop all your local changes and commits, fetch the latest history from the server and point your local master branch at it like this
**git fetch origin**
**git reset --hard origin/master**

useful hints
built-in git GUI
**gitk**
use colorful git output
**git config color.ui true**
show log on just one line per commit
**git config format.pretty oneline**
use interactive adding
**git add -i**

---
Để Git không theo dõi loại file nào, thì đưa nó vào file .gitignore trong thư mục chính của repo. Ví dụ: *.log.

Trong Windows, không tạo được file không tên (dạng file ẩn của Linux) thì dùng lệnh của Linux để tạo:
touch .gitignore
-> nếu file này chưa tồn tại thì sẽ tạo file này; còn nếu file đã có thì nó cập nhật date time mới cho file.

File .gitignore khi chỉnh sửa trong branch, thì khi chuyển sang master Git thì phải merge branch để cập nhật .gitignore của master => file hoặc thư mục ẩn cũng được git track?

---
Revert:

How can I rollback a github repository to a specific commit?

git reset --hard <old-commit-id>
git push -f <remote-name> <branch-name>
http://stackoverflow.com/questions/4372435/how-can-i-rollback-a-github-repository-to-a-specific-commit


If you haven't pushed this anywhere, you can use git reset --hard HEAD^ to throw away the latest commit (this also throws away any uncommitted changes, so be sure you don't have any you want to save). Assuming you're ok with the sensitive information being present in your copy and nobody else's, you're done. You can continue to work and a subsequent git push won't push your bad commit.

If that's not a safe assumption (though if not I'd love to hear why), then you need to expire your reflogs and force a garbage collection that collects all outstanding objects right now. You can do that with

git reflog expire --expire=now --expire-unreachable=now --all
git gc --prune=now
though this should only be done if you really absolutely need to do it.

If you have pushed your commit, then you're pretty much out of luck. You can do a force-push to revert it remotely (though only if the remote side allows that), but **you can't delete the commit itself from the remote side's database**, so anyone who has access to that repository can find it if they know what to look for.
http://stackoverflow.com/questions/8903953/git-revert-last-commit-and-remove-it-from-history

---
This will unstage all files you might have staged with git add:
**git reset**
This will revert all local uncommitted changes (should be executed in repo root):

**git checkout .**
You can also revert uncommitted changes only to particular file or directory:

**git checkout [some_dir|file.txt]**
Yet another way to **revert all uncommitted changes** (longer to type, but works from any subdirectory):
**git reset --hard HEAD**
This will remove all local untracked files, so only git tracked files remain:

**git clean -fd**

-f is "force"
-d is "include directory"

or: git clean -fdx
-x is "include all ignored files" (careful to use)

To sum it up: executing commands below is basically equivalent to fresh git clone from original source (but it does not re-download anything, so is much faster):

git reset
git checkout .
git clean -fdx
---
If you wish to "undo" all uncommitted changes simply run:

**git stash**
git stash drop
http://stackoverflow.com/questions/14075581/git-undo-all-uncommitted-changes
---
https://github.com/blog/2019-how-to-undo-almost-anything-with-git
---
submodule repo
For submodules its a bit tricky.

Try updating the submodules if you think you did not make any local changes:
**git submodule update --init**
If it doesn't work, hard reset all submodules:
**git submodule foreach git reset --hard**
http://kalyanchakravarthy.net/blog/git-discard-submodule-changes.html

---

https://orga.cat/posts/most-useful-git-commands

Here there are some examples of git commands that I use often.

Not all commands written here are git commands, but all of them are related to git. Please refer to the documentation for more details.

Set your details
git config --global user.name "John Doe"
git config --global user.email "john@example.com"
Use --global to set the configuration for all projects. If git config is used without --global and run inside a project directory, the settings are set for the specific project.

Make git ignore file modes
cd project/
git config core.filemode false
This option is useful if the file permissions are not important to us, for example when we are on Windows.

See your settings
git config --list
Initialize a git repository for existing code
cd existing-project/
git init
Clone a remote repository
git clone https://github.com/user/repository.git
This creates a new directory with the name of the repository.

Clone a remote repository in the current directory
git clone https://github.com/user/repository.git .
Get help for a specific git command
git help clone
Update and merge your current branch with a remote
cd repository/
git pull origin master
Where origin is the remote repository, and master the remote branch.
If you don't want to merge your changes, use git fetch

View remote urls
git remote -v
Change origin url
git remote set-url origin http//github.com/repo.git
Add remote
git remote add remote-name https://github.com/user/repo.git
See non-staged (non-added) changes to existing files
git diff
Note that this does not track new files.

See staged, non-commited changes
git diff --cached
See differences between local changes and master
git diff origin/master
Note that origin/master is one local branch, a shorthand for refs/remotes/origin/master, which is the full name of the remote-tracking branch.

See differences between two commits
git diff COMMIT1_ID COMMIT2_ID
See the files changed between two commits
git diff --name-only COMMIT1_ID COMMIT2_ID
See the files changed in a specific commit
git diff-tree --no-commit-id --name-only -r COMMIT_ID
or

git show --pretty="format:" --name-only COMMIT_ID
source: http://stackoverflow.com/a/424142/1391963

See diff before push
git diff --cached origin/master
See details (log message, text diff) of a commit
git show COMMIT_ID
Check the status of the working tree (current branch, changed files...)
git status
Make some changes, commit them
git add changed_file.txt
git add folder-with-changed-files/
git commit -m "Commiting changes"
Rename/move and remove files
git rm removeme.txt tmp/crap.txt
git mv file_oldname.txt file_newname.txt
git commit -m "deleting 2 files, renaming 1"
Change message of last commit
git commit --amend -m "New commit message"
Push local commits to remote branch
git push origin master
See recent commit history
git log
See commit history for the last two commits
git log -2
See commit history for the last two commits, with diff
git log -p -2
See commit history printed in single lines
git log --pretty=oneline
Revert one commit, push it
git revert dd61ab21
git push origin master
Revert to the moment before one commit
# reset the index to the desired tree
git reset 56e05fced

# move the branch pointer back to the previous HEAD
git reset --soft HEAD@{1}

git commit -m "Revert to 56e05fced"

# Update working copy to reflect the new commit
git reset --hard
Source: http://stackoverflow.com/q/1895059/1391963

Undo last commit, preserving local changes
git reset --soft HEAD~1
Undo last commit, without preserving local changes
git reset --hard HEAD~1
Undo last commit, preserving local changes in index
git reset --mixed HEAD~1
Or git reset HEAD~1
See also http://stackoverflow.com/q/927358/1391963

Undo non-pushed commits
git reset origin/master
Reset to remote state
git fetch origin
git reset --hard origin/master
See local branches
git branch
See all branches
git branch -a
Make some changes, create a patch
git diff > patch-issue-1.patch
Add a file and create a patch
git add newfile
git diff --staged > patch-issue-2.patch
Add a file, make some changes, and create a patch
git add newfile
git diff HEAD > patch-issue-2.patch
Make a patch for a commit
git format-patch COMMIT_ID
Make patches for the last two commits
git format-patch HEAD~2
Make patches for all non-pushed commits
git format-patch origin/master
Create patches that contain binary content
git format-patch --binary --full-index origin/master
Apply a patch
git apply -v patch-name.patch
Apply a patch created using format-patch
git am patch1.patch
Create a tag
git tag 7.x-1.3
Push a tag
git push origin 7.x-1.3
Create a branch
git checkout master
git branch new-branch-name
Here master is the starting point for the new branch. Note that with these 2 commands we don't move to the new branch, as we are still in master and we would need to run git checkout new-branch-name. The same can be achieved using one single command: git checkout -b new-branch-name

Checkout a branch
git checkout new-branch-name
See commit history for just the current branch
git cherry -v master
(master is the branch you want to compare)

Merge branch commits
git checkout master
git merge branch-name
Here we are merging all commits of branch-name to master.

Merge a branch without committing
git merge branch-name --no-commit --no-ff
See differences between the current state and a branch
git diff branch-name
See differences in a file, between the current state and a branch
git diff branch-name path/to/file
Delete a branch
git branch -d new-branch-name
Push the new branch
git push origin new-branch-name
Get all branches
git fetch origin
Get the git root directory
git rev-parse --show-toplevel
Source: http://stackoverflow.com/q/957928/1391963

Remove from repository all locally deleted files
git rm $(git ls-files --deleted)
Source: http://stackoverflow.com/a/5147119/1391963

Delete all untracked files
git clean -f
Including directories:

git clean -f -d
Preventing sudden cardiac arrest:

git clean -n -f -d
Source: http://stackoverflow.com/q/61212/1391963

Show total file size difference between two commits
Short answer: Git does not do that.
Long answer: See http://stackoverflow.com/a/10847242/1391963

Unstage (undo add) files:
git reset HEAD file.txt
See closest tag
git describe --tags `git rev-list --tags --max-count=1`
Source. See also git-describe.

Have git pull running every X seconds, with GNU Screen
screen
for((i=1;i<=10000;i+=1)); do sleep 30 && git pull; done
Use Ctrl+a Ctrl+d to detach the screen.

See previous git commands executed
history | grep git
or

grep '^git'  /root/.bash_history
See recently used branches (i.e. branches ordered by most recent commit)
git for-each-ref --sort=-committerdate refs/heads/ | head
Source: http://stackoverflow.com/q/5188320/1391963

Tar project files, excluding .git directory
cd ..
tar cJf project.tar.xz project/ --exclude-vcs
Tar all locally modified files
git diff --name-only | xargs tar -cf project.tar -T -
Look for conflicts in your current files
grep -H -r "<<<" *
grep -H -r ">>>" *
grep -H -r '^=======$' *
There's also git-grep.

Git making patch and apply:
+ Creating the patch:
git format-patch master --stdout > a_file.patch
or
git diff > a_file.patch
+ Applying the patch in a file:
First the stats: git apply --stat a_file.patch
Then a dry run to detect errors: git apply --check a_file.patch
And apply: git apply a_file.patch

Apply a patch not using git:
patch < file.patch


