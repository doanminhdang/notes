Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2019-04-26T16:31:37+02:00

====== Thunderbird ======
Created Tuesday 02 October 2018

Back up and restore Thunderbird profile:

Backing up a profile
To back up your profile, first close Thunderbird if it is open and then copy the profile folder to another location.

Shut down Thunderbird.
Locate your profile folder, as explained above.
Go to one level above your profile's folder, i.e. to ~/.mozilla/Thunderbird/
Right-click on your profile folder (e.g. xxxxxxxx.default), and select Copy.
Right-click the backup location (e.g. a USB-stick or a blank CD-RW disc), and select Paste.
Restoring a profile backup
Shut down Thunderbird.
If your existing profile folder and profile backup folder have the same name, simply replace the existing profile folder with the profile backup, then start Thunderbird.
Important: The profile folder names must match exactly for this to work, including the random string of 8 characters. If the names do not match or if you are restoring a backup to a different location, follow the steps below.
Restoring to a different location
If the profile folder names do not match or if you want to move or restore a profile to a different location, do the following:

Completely close Thunderbird, as explained above.
Use the Thunderbird Profile Manager to create a new profile in your desired location, then exit the Profile Manager.
Note: If you just installed Thunderbird on a new computer, you can use the default profile that is automatically created when you first run Thunderbird, instead of creating a new profile.
Locate the backed up profile folder on your hard drive or backup medium (e.g., your USB-stick).
Open the profile folder backup (e.g., the xxxxxxxx.default backup).
Copy the entire contents of the profile folder backup, such as the mimeTypes.rdf file, prefs.js file, etc.
Locate and open the new profile folder as explained above and then close Thunderbird (if open).
Paste the contents of the backed up profile folder into the new profile folder, overwriting existing files of the same name.
Start Thunderbird.
https://support.mozilla.org/en-US/kb/profiles-where-thunderbird-stores-user-data#w_backing-up-a-profile
