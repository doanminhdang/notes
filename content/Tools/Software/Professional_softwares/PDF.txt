Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2018-09-27T16:02:06+02:00

====== PDF ======
Created Thursday 27 September 2018

Để mở khóa file PDF, dùng công cụ qpdf, đặt mật khẩu trong dấu ngoặc đơn để tránh lỗi gây ra do ký tự lạ như $:
qpdf --decrypt --password='mat_khau' file.pdf abc.pdf

Conver PDF to DOC, using Software: LibreOffice in Linux:
soffice --infilter="writer_pdf_import" --convert-to doc filename.pdf
other command, seems old:
lowriter --invisible --convert-to doc '/your/file.pdf'

Embed PDF in HTML:

Probably the best approach is to use the PDF.JS library. It's a pure HTML5/JavaScript renderer for PDF documents without any third-party plugins.
Online demo: http://mozilla.github.com/pdf.js/web/viewer.html
GitHub: https://github.com/mozilla/pdf.js
Ref: https://stackoverflow.com/questions/291813/recommended-way-to-embed-pdf-in-html
