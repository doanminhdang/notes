Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2016-11-25T17:54:42+01:00

====== Quick fix pull request ======
Created Freitag 25 November 2016

Học cách dùng git khi làm việc với acados cùng Andrea Zanelli:

Ví dụ: đang làm việc ở master của bản fork của mình (origin/master), muốn sửa nhanh một lỗi ở bản upstream (upstream/master), rồi sau đó lại quay lại bản fork của mình để làm tiếp. Cách làm:

Bước 1: Tạo một clean branch là copy vào thời điểm hiện tại của upstream/master

$git pull upstream/master
-> để bản branch upstream/master trên repo ở máy của mình được sync với repo remote upstream/master

$ git checkout upstream/master
-> sẽ tạo ra một temporary copy của upstream/master, chuyển vào đó (gọi là HEAD upstream/master), sau khi chuyển ra khỏi branch này thì nó sẽ tự bị xóa đi

$ git checkout -b quickfix
-> sẽ tạo ra branch quickfix ở repo local, là copy từ bản HEAD upstream/master, để mình có thể lưu lại mà không bị mất

Bước 2: Bây giờ làm việc ở branch quickfix này, để sửa file nào cần fix nhanh lên upstream/master. Sau khi đã sửa ở branch quickfix, cần add file đã sửa, commit vào local repo. Rồi push branch mới lên remote:

$ git push origin quickfix
 
Bước 3: Sau đó làm lệnh "Pull request" bằng trình duyệt web: vào repo upstream ở trang web remote (github.com), nhấn nút "Pull request", chọn "compare to different forks", rồi so sánh branch master của upstream với branch quickfix trên bản fork của mình
-> điền thêm comment cho cái pull request, rồi nhấn xác nhận.

Ở repo acados trên Github, có cài đặt thêm travis để tự động biên dịch đối với mỗi bản pull request mới (nếu có lỗi sẽ tự động gửi email báo cho những người có writing permissions).


