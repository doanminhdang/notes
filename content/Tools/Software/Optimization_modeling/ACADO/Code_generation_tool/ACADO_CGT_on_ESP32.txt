Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-02-24T10:09:36+01:00

====== ACADO CGT on ESP32 ======
Created Freitag 24 Februar 2017

*** Need to use -std=gnu99 instead of -std=c99**
(esp codes will yield errors with -std=c99)

Problem appears when compiling ACADO code generation **using qpOASES3** with ESP32:

QPOASES_EPS is defined at different places:
main/include/Constants.h:  static const real_t QPOASES_EPS = 1.193e-07;
main/include/Constants.h:  static const real_t QPOASES_EPS = 2.221e-16;
main/include/acado_qpoases3_interface.h:#define QPOASES_EPS        2.221e-16

-> solution: use the flag -D____CODE-GENERATION____ when compiling C code.


/home/dang/esp/myapp/main/./QProblemB.c: In function 'QProblemB_backsolveRrem':
/home/dang/esp/myapp/main/include/Types.h:145:28: error: array subscript is above array bounds [-Werror=array-bounds]
 #define RR( I,J )  _THIS->R[(I)+NVMAX*(J)]
							^
/home/dang/esp/myapp/main/./QProblemB.c:1586:12: note: in expansion of macro 'RR'
	 sum -= RR(i,j) * a[j];
			^
/home/dang/esp/myapp/main/./QProblemB.c: In function 'QProblemB_determineStepDirection':
/home/dang/esp/myapp/main/include/Types.h:145:28: error: array subscript is above array bounds [-Werror=array-bounds]
 #define RR( I,J )  _THIS->R[(I)+NVMAX*(J)]
							^
cc1: some warnings being treated as errors

**# Dang add warning flags in project_acado.mk for compiling ACADO:** 
**-Wno-error=array-bounds**


*** Using qpdunes-dev:**

[-Werror=maybe-uninitialized]
cc1: some warnings being treated as errors

**# Dang add for compiling ACADO: **
**-Wno-error=maybe-uninitialized \**
**-Wno-error=unknown-pragmas**

---

make app
...
App built. Default flash app command is:
python /home/dang/esp/esp-idf/components/esptool_py/esptool/esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 115200 write_flash --flash_mode dio --flash_freq 40m --flash_size 2MB 0x10000 /home/dang/esp/acado_qpoases3/build/app-template.bin

make all
...

To flash all build output, run 'make flash' or:
python /home/dang/esp/esp-idf/components/esptool_py/esptool/esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 115200 write_flash --flash_mode dio --flash_freq 40m --flash_size 2MB 0x1000 /home/dang/esp/acado_qpoases3/build/bootloader/bootloader.bin 0x10000 /home/dang/esp/acado_qpoases3/build/app-template.bin 0x8000 /home/dang/esp/acado_qpoases3/build/partitions_singleapp.bin

To flash all build output, run 'make flash' or:
python /home/dang/esp/esp-idf/components/esptool_py/esptool/esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 115200 write_flash --flash_mode dio --flash_freq 40m --flash_size 2MB 0x1000 /home/dang/esp/acado_qpdunes/build/bootloader/bootloader.bin 0x10000 /home/dang/esp/acado_qpdunes/build/app-template.bin 0x8000 /home/dang/esp/acado_qpdunes/build/partitions_singleapp.bin

sudo python /home/dang/esp/esp-idf/components/esptool_py/esptool/esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 115200 write_flash --flash_mode dio --flash_freq 40m --flash_size 4MB 0x1000 /home/dang/ownCloud/IMTEK-Freiburg/Projects/projects_esp/acado_qpdunes/build/bootloader/bootloader.bin 0x10000 /home/dang/ownCloud/IMTEK-Freiburg/Projects/projects_esp/acado_qpdunes/build/app-template.bin 0x8000 /home/dang/ownCloud/IMTEK-Freiburg/Projects/projects_esp/acado_qpdunes/build/partitions_singleapp.bin

-----
New procedure after updating esp-idf (2017.03.20):
python /home/dang/esp/esp-idf/components/esptool_py/esptool/esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 115200 **--before default_reset --after hard_reset** write_flash -u --flash_mode dio --flash_freq 40m --flash_size detect 0x10000 /home/dang/ownCloud/IMTEK-Freiburg/Projects/projects_esp/acados_test_ocp_qp/build/app-template.bin

---
qpdunes:
assertion "foo" failed: file "/home/dang/esp/acado_qpdunes/main/./qpdunes_utils.c", line 59, functionc
Guru Meditation Error: Core   0 panic'ed (Unhandled debug exception)
Register dump:
PC      :  40056f2a  PS      :  00060b36  A0      :  8008329f  A1      :  3ffb9440  
A2      :  00000076  A3      :  3f402613  A4      :  3f4043a8  A5      :  3f4043ac  
A6      :  0000003b  A7      :  00000008  A8      :  80056f28  A9      :  3ffb9380  
A10     :  00000076  A11     :  3ffb796c  A12     :  00000000  A13     :  3f402641  
A14     :  00000001  A15     :  0000000e  SAR     :  0000001f  EXCCAUSE:  00000001  
EXCVADDR:  00000000  LBEG    :  4000c46c  LEND    :  4000c477  LCOUNT  :  00000000  
Rebooting...

qpdunes-dev:

Guru Meditation Error of type StoreProhibited occurred on core   0. Exception was unhandled.
Register dump:
PC      :  400e7b5f  PS      :  00060930  A0      :  800eaf20  A1      :  3ffba270  
A2      :  00000000  A3      :  00000005  A4      :  00000005  A5      :  00000018  
A6      :  3f407488  A7      :  00000010  A8      :  800d1108  A9      :  3ffba270  
A10     :  00000018  A11     :  0000000f  A12     :  3ffb1754  A13     :  80000000  
A14     :  00000000  A15     :  00000004  SAR     :  0000001f  EXCCAUSE:  0000001d  
EXCVADDR:  00000000  LBEG    :  4000c46c  LEND    :  4000c477  LCOUNT  :  00000000  
Rebooting...

Using GDB to debug:

**qpDUNES_allocInterval** (qpData=0x3ffb5c98 <qpData>, nX=4, nU=<optimized out>, nV=5, nD=0)
	**at /home/dang/esp/acado_qpdunes-dev/main/./setup_qp.c:212**
212		interval->nV = nV;
(gdb) list
207										)
208	{
209		interval_t* interval = (interval_t*)calloc( 1,sizeof(interval_t) );
210	
211		interval->nD = nD;
212		interval->nV = nV;
213	
214		interval->H.data = (real_t*)calloc( nV*nV,sizeof(real_t) );
215		interval->H.sparsityType = QPDUNES_MATRIX_UNDEFINED;
216		interval->cholH.data = (real_t*)calloc( nV*nV,sizeof(real_t) );
(gdb) bt
#0  qpDUNES_allocInterval (qpData=0x3ffb5c98 <qpData>, nX=4, nU=<optimized out>, nV=5, nD=0)
	at /home/dang/esp/acado_qpdunes-dev/main/./setup_qp.c:212
#1  0x400e24b8 in qpDUNES_setup (qpData=0x3ffb5c98 <qpData>, nI=10, nX=4, nU=1, nD=0x3ffba740, 
	options=<optimized out>) at /home/dang/esp/acado_qpdunes-dev/main/./setup_qp.c:81
#2  0x400da05f in initializeQpDunes () at /home/dang/esp/acado_qpdunes-dev/main/./acado_solver.c:60
#3  0x400dc232 in acado_initializeSolver ()
	at /home/dang/esp/acado_qpdunes-dev/main/./acado_solver.c:593
#4  0x400d9e13 in app_main ()
	at /home/dang/ownCloud/IMTEK-Freiburg/Projects/projects_esp/acado_qpdunes-dev/main/./main.c:72


qpoases3:
Some error during computation, like:
Guru Med0;�?�:�?ErroP��? type  ��?  0.��?<R������
-> then reboot.

Using GDB to debug, found:
#0  0x400822f0 in prvInsertBlockIntoFreeList (pxBlockToInsert=0x3fddb4ec)
	at /home/dang/esp/esp-idf/components/freertos/./heap_regions.c:433
#1  pvPortMallocTagged (xWantedSize=144, tag=0)
	at /home/dang/esp/esp-idf/components/freertos/./heap_regions.c:290
#2  0x400d0da8 in pvPortMallocCaps (caps=4, xWantedSize=132)
	at /home/dang/esp/esp-idf/components/esp32/./heap_alloc_caps.c:256
#3  pvPortMalloc (xWantedSize=132) at /home/dang/esp/esp-idf/components/esp32/./heap_alloc_caps.c:230
#4  0x4008187e in _calloc_r (r=0x3ffbaccc, count=<optimized out>, size=33)
	at /home/dang/esp/esp-idf/components/newlib/./syscalls.c:65
#5  0x40117aaa in _Balloc (ptr=0x3ffbaccc, k=1) at ../../../.././newlib/libc/stdlib/mprec.c:106
#6  0x401180fc in __d2b (ptr=0x3ffbaccc, _d=1, e=0x3ffba680, bits=0x3ffba684)
	at ../../../.././newlib/libc/stdlib/mprec.c:785
#7  0x40116940 in _dtoa_r (ptr=0x3ffbaccc, _d=<optimized out>, mode=2, ndigits=16, 
	decpt=decpt@entry=0x3ffba788, sign=sign@entry=0x3ffba724, rve=rve@entry=0x3ffba720)
	at ../../../.././newlib/libc/stdlib/dtoa.c:291
#8  0x40114f9c in __cvt (data=0x3ffbaccc, value=1, ndigits=16, flags=1024, 
	sign=sign@entry=0x3ffba78c "", decpt=decpt@entry=0x3ffba788, ch=ch@entry=101, 
	length=length@entry=0x3ffba784, buf=buf@entry=0x0)
	at ../../../.././newlib/libc/stdio/nano-vfprintf_float.c:101
#9  0x4011519d in _printf_float (data=0x3ffbaccc, pdata=0x3ffba7f0, fp=0x3ffba880, pfunc=0x40056f2c, 
	ap=<optimized out>) at ../../../.././newlib/libc/stdio/nano-vfprintf_float.c:236
#10 0x4000bf14 in ?? ()
#11 0x400572fc in ?? ()
#12 0x40056ba5 in ?? ()
**#13 0x400fd828 in QProblem_performStep** (_THIS=0x3ffbad00, delta_g=0x3ffb3618 <delta_g>, 
	delta_lbA=0x3ffb35c8 <delta_lbA$6487>, delta_ubA=0x3ffb3578 <delta_ubA$6488>, 
---Type <return> to continue, or q <return> to quit---
	delta_lb=0x3ffb3528 <delta_lb>, delta_ub=0x3ffb34d8 <delta_ub>, 
	delta_xFX=delta_xFX@entry=0x3ffb3488 <delta_xFX$6481>, 
	delta_xFR=delta_xFR@entry=0x3ffb3438 <delta_xFR$6480>, 
	delta_yAC=delta_yAC@entry=0x3ffb33e8 <delta_yAC$6482>, 
	delta_yFX=delta_yFX@entry=0x3ffb3398 <delta_yFX$6483>, BC_idx=BC_idx@entry=0x3ffbaa38, 
	BC_status=BC_status@entry=0x3ffbaa34, BC_isBound=BC_isBound@entry=0x3ffbaa30)
	**at /home/dang/esp/acado_qpoases3/main/./QProblem.c:5959**
#14 0x4010afc5 in QProblem_solveQP (_THIS=0x3ffbad00, g_new=0x3ffb3848 <g_original>, 
	lb_new=0x3ffb3bd0 <lb_new_far>, ub_new=0x3ffb3b80 <ub_new_far>, 
	lbA_new=0x3ffb3b30 <lbA_new_far$5946>, ubA_new=0x3ffb3ae0 <ubA_new_far$5945>, 
	nWSR=nWSR@entry=0x3ffb1df0 <acado_nWSR>, cputime=cputime@entry=0x0, 
	nWSRperformed=nWSRperformed@entry=0, isFirstCall=isFirstCall@entry=BT_TRUE)
	at /home/dang/esp/acado_qpoases3/main/./QProblem.c:2542
#15 0x4010bfc5 in QProblem_solveRegularisedQP (_THIS=0x3ffbad00, g_new=0x3ffb3848 <g_original>, 
	lb_new=0x3ffb3bd0 <lb_new_far>, ub_new=0x3ffb3b80 <ub_new_far>, 
	lbA_new=0x3ffb3b30 <lbA_new_far$5946>, ubA_new=0x3ffb3ae0 <ubA_new_far$5945>, 
	nWSR=nWSR@entry=0x3ffb1df0 <acado_nWSR>, cputime=cputime@entry=0x0, 
	nWSRperformed=nWSRperformed@entry=0, isFirstCall=isFirstCall@entry=BT_TRUE)
	at /home/dang/esp/acado_qpoases3/main/./QProblem.c:2678
#16 0x4010c4de in QProblem_hotstart (_THIS=0x3ffbad00, g_new=0x3ffb3848 <g_original>, 
	lb_new=0x3ffb37f8 <lb_original>, ub_new=0x3ffb37a8 <ub_original>, 
	lbA_new=0x3ffb3758 <lbA_original$6442>, ubA_new=0x3ffb3708 <ubA_original$6443>, 
	nWSR=0x3ffb1df0 <acado_nWSR>, cputime=0x0) at /home/dang/esp/acado_qpoases3/main/./QProblem.c:675
#17 0x4010d947 in QProblem_solveInitialQP (_THIS=0x3ffbad00, xOpt=<optimized out>, 
	yOpt=<optimized out>, guessedBounds=0x0, guessedConstraints=0x0, _R=0x3ffba870, 
	nWSR=nWSR@entry=0x3ffb1df0 <acado_nWSR>, cputime=cputime@entry=0x0)
---Type <return> to continue, or q <return> to quit---
	at /home/dang/esp/acado_qpoases3/main/./QProblem.c:2387
#18 0x4010dccc in QProblem_initW (_THIS=0x3ffbad00, _H=0x3ffb7150 <acadoWorkspace+7752>, 
	_g=<optimized out>, _A=0x3ffb7470 <acadoWorkspace+8552>, _lb=0x3ffb77e0 <acadoWorkspace+9432>, 
	_ub=0x3ffb7831 <acadoWorkspace+9513>, _lbA=_lbA@entry=0x3ffb7880 <acadoWorkspace+9592>, 
	_ubA=0x3ffb78d0 <acadoWorkspace+9672>, nWSR=nWSR@entry=0x3ffb1df0 <acado_nWSR>, 
	cputime=cputime@entry=0x0, xOpt=xOpt@entry=0x0, 
	yOpt=yOpt@entry=0x3ffb7970 <acadoWorkspace+9832>, guessedBounds=guessedBounds@entry=0x0, 
	guessedConstraints=guessedConstraints@entry=0x0, _R=0x3fddb44c, _R@entry=0x0)
	at /home/dang/esp/acado_qpoases3/main/./QProblem.c:465
#19 0x400eddc2 in acado_solve ()
	at /home/dang/esp/acado_qpoases3/main/./acado_qpoases3_interface.c:49
#20 0x400eb895 in acado_feedbackStep () at /home/dang/esp/acado_qpoases3/main/./acado_solver.c:1398
#21 0x400d9b00 in app_main ()
	at /home/dang/ownCloud/IMTEK-Freiburg/Projects/projects_esp/acado_qpoases3/main/./main.c:101
