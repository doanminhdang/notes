Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-02-10T09:40:45+01:00

====== Python interface ======
Created Freitag 10 Februar 2017

Install some new packages:
+ swig
+ python3-dev
+ numpy
sudo pip3 install numpy

Robin chose swig because it also helps interfacing C code to MATLAB/Octave (while Cython only interfaces C and Python).
