Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2016-11-30T12:12:17+01:00

====== acados ======
Created Mittwoch 30 November 2016

Khi BLASFEO (hoặc qpOASES) dùng OpenBLAS, cần cài đặt openblas vào máy, build từ https://github.com/xianyi/OpenBLAS

Hiện nay mặc định Open BLAS cài vào địa chỉ: [[/opt/OpenBLAS]]

Còn dòng thiết lập trong file CMakeLists.txt của acados thì chỉ đến địa chỉ [[/opt/openblas]]
-> cần làm một cái symlink để /opt/openblas chỉ đến /opt/OpenBLAS (hoặc đổi tên thư mục)

---

Paper to write:

Benchmark on embedded platforms:
Gianluca: ARM Cortex A8 is bad (in supporting double precision float) -> should go to A9.


cmake -DCMAKE_BUILD_TYPE=Test

---
 Từ 2/11/2017: qpOASES được fork thành một submodule trong acados (Branimir tạo github repo cho qpoases), cần được chạy lệnh submodule init và submodule update.
Gọi là acados version 2, có thay đổi cả interface với Python
